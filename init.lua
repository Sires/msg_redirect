players = {}
switch2 = false

local register_on_message = core.register_on_sending_chat_message
if core.register_on_sending_chat_messages then
	register_on_message = core.register_on_sending_chat_messages
end

local function Set(list)
    local nlist = {}
    for _, item in ipairs(list) do nlist[item] = true end
    return nlist
end

local function canTalk()
	if core.get_privilege_list then
		return core.get_privilege_list().shout
	else
		return true
	end
end

local function say(message)
	if not canTalk() then
		minetest.display_chat_message("You need \"shout\" in order to talk")
		return
	end
	minetest.send_chat_message(message)
	if minetest.get_server_info().protocol_version < 29 then
		local name = minetest.localplayer:get_name()
		minetest.display_chat_message("<"..name.."> " .. message)
	end
end

register_on_message(function(message)
	if message:sub(1,1) == "/" then
		return false
	end
    if message:sub(1, 1) == ">" then
        say(message:sub(2, #message))
        return true
    elseif switch2 and players ~= {} then
        for _, player in ipairs(players) do
            minetest.run_server_chatcommand("msg", player .. " " .. message)
        end
        return true, message
    end
	return false
end)

core.register_chatcommand("*", {
    description = core.gettext("Turns on and off the auto /msg redirect"),
    func = function(_)
        switch2 = not switch2
        local t = "off"
        if switch2 then
            t = "on"
        end
        return true, "The /msg redirect is now " .. t .. "."
    end
})

core.register_chatcommand("set_players", {
	description = core.gettext("Change chat colour"),
	func = function(msg)
		local nplayers = string.split(msg, " ")
        for _, player in ipairs(nplayers) do
            if not (Set(minetest.get_player_names())[player]) then
                return true, "Could not set players, the player " .. player .. " doesn't exist."
            end
        end
        players = nplayers
		return true, "Players setted."
	end,
})
